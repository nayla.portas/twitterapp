package com.nayla.twitter_app.core.domain

data class FollowData(val nickname: String, val nicknameToFollow: String)