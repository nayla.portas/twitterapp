package com.nayla.twitter_app.core.action

import com.nayla.twitter_app.core.domain.TweetData
import com.nayla.twitter_app.core.repository.UserService
import com.nayla.twitter_app.infrastructure.TwitterException

class SendTweet(private val userService: UserService) {

    suspend operator fun invoke(nickname: String, text: String) {
        if(nickname.isEmpty() || text.isEmpty())
            throw TwitterException.FieldEmptyException()
        userService.sendTweet(TweetData(nickname, text))
    }
}
