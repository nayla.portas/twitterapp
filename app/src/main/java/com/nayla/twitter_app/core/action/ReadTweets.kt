package com.nayla.twitter_app.core.action


import com.nayla.twitter_app.core.repository.UserService

class ReadTweets(private val userService: UserService) {

    suspend operator fun invoke(nickname: String) : List<String> {
        return userService.tweets(nickname)
    }
}
