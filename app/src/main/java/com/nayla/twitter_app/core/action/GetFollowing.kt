package com.nayla.twitter_app.core.action

import com.nayla.twitter_app.core.repository.UserService

class GetFollowing(private val userService: UserService) {

    suspend operator fun invoke(nickname: String) : List<String> {
        return userService.following(nickname)
    }
}
