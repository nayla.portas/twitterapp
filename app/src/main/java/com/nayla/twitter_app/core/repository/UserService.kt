package com.nayla.twitter_app.core.repository

import com.nayla.twitter_app.core.domain.FollowData
import com.nayla.twitter_app.core.domain.TweetData
import com.nayla.twitter_app.core.domain.UserData

interface UserService {
    suspend fun add(user: UserData)
    suspend fun sendTweet(tweet: TweetData)
    suspend fun tweets(nickname: String): List<String>
    suspend fun followUser(followData: FollowData)
    suspend fun following(nickname: String): List<String>
}
