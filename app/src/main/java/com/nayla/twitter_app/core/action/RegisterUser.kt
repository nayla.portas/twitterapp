package com.nayla.twitter_app.core.action

import com.nayla.twitter_app.core.domain.UserData
import com.nayla.twitter_app.core.repository.UserService
import com.nayla.twitter_app.infrastructure.TwitterException

class RegisterUser(private val userService: UserService) {

    suspend operator fun invoke(nickname: String, name: String) {
        if(nickname.isEmpty() || name.isEmpty())
            throw TwitterException.FieldEmptyException()
        userService.add(UserData(nickname, name))
    }
}
