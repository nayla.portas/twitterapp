package com.nayla.twitter_app.core.domain

data class UserData(val nickname: String, val name: String)
