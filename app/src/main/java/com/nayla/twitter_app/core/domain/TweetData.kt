package com.nayla.twitter_app.core.domain

data class TweetData(val nickname: String, val text: String)
