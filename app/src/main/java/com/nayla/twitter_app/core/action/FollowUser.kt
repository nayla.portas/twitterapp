package com.nayla.twitter_app.core.action

import com.nayla.twitter_app.core.domain.FollowData
import com.nayla.twitter_app.core.repository.UserService
import com.nayla.twitter_app.infrastructure.TwitterException

class FollowUser(private val userService: UserService) {

    suspend operator fun invoke(nickname: String, nicknameToFollow: String) {
        if(nickname.isEmpty() || nicknameToFollow.isEmpty())
            throw TwitterException.FieldEmptyException()
        userService.followUser(FollowData(nickname, nicknameToFollow))
    }
}
