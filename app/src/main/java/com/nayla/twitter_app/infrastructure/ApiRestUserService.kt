package com.nayla.twitter_app.infrastructure


import com.nayla.twitter_app.core.domain.FollowData
import com.nayla.twitter_app.core.domain.TweetData
import com.nayla.twitter_app.core.domain.UserData
import com.nayla.twitter_app.core.repository.UserService

class ApiRestUserService(private val apiRestClient: ApiRestClient) : UserService {

    override suspend fun add(user: UserData) {
        kotlin.runCatching {
            apiRestClient.registerUser(user)
        }.getOrElse {
            throw TwitterException.TwitterRegisterException()
        }
    }

    override suspend fun sendTweet(tweet: TweetData) {
        kotlin.runCatching {
            apiRestClient.sendTweet(tweet)
        }.getOrElse {
            throw TwitterException.TwitterSendTweetException()
        }
    }

    override suspend fun tweets(nickname: String): List<String> {
        return kotlin.runCatching {
           apiRestClient.readTweets(nickname)
        }.getOrElse {
            throw TwitterException.TwitterReadTweetsException()
        }
    }

    override suspend fun followUser(followData: FollowData) {
        kotlin.runCatching {
            apiRestClient.followUser(followData)
        }.getOrElse {
            throw TwitterException.TwitterFollowException()
        }
    }

    override suspend fun following(nickname: String): List<String> {
        return kotlin.runCatching {
            apiRestClient.following(nickname)
        }.getOrElse {
            throw TwitterException.TwitterGetFollowingException()
        }
    }
}
