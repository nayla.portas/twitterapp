package com.nayla.twitter_app.infrastructure

import com.nayla.twitter_app.core.domain.FollowData
import com.nayla.twitter_app.core.domain.TweetData
import com.nayla.twitter_app.core.domain.UserData
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiRestClient {

    @POST("user/register")
    suspend fun registerUser(@Body user: UserData)

    @POST("user/tweet")
    suspend fun sendTweet(@Body tweet: TweetData)

    @GET("user/{nickname}/tweets")
    suspend fun readTweets(@Path("nickname") nickname: String): List<String>

    @POST("user/follow")
    suspend fun followUser(@Body followData: FollowData)

    @GET("user/{nickname}/following")
    suspend fun following(@Path("nickname") nickname: String): List<String>
}
