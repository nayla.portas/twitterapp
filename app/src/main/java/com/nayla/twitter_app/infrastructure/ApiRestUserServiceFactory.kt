package com.nayla.twitter_app.infrastructure



object ApiRestUserServiceFactory {

    private val apiRestUserService: ApiRestUserService by lazy { buildService() }

    private fun buildService() : ApiRestUserService {
        return ApiRestUserService(ClientFactory.getApiClient())
    }

    fun getService() : ApiRestUserService {
        return apiRestUserService
    }
}
