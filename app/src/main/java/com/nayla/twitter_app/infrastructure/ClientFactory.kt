package com.nayla.twitter_app.infrastructure

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ClientFactory {

    private val apiClient: Retrofit by lazy { buildClient() }

    private fun buildClient(): Retrofit = Retrofit.Builder()
        .baseUrl("http://10.0.2.2:8080/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getApiClient() : ApiRestClient {
        return apiClient.create(ApiRestClient::class.java)
    }
}
