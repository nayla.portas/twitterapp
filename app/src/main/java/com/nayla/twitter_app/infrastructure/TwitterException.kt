package com.nayla.twitter_app.infrastructure


sealed class TwitterException : Throwable() {
    class TwitterRegisterException : TwitterException()
    class TwitterReadTweetsException : TwitterException()
    class TwitterGetFollowingException : TwitterException()
    class TwitterFollowException : TwitterException()
    class TwitterSendTweetException: TwitterException()
    class FieldEmptyException : TwitterException()
}
