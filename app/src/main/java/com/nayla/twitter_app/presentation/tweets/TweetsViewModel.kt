package com.nayla.twitter_app.presentation.tweets

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nayla.twitter_app.core.action.ReadTweets
import kotlinx.coroutines.launch

class TweetsViewModel(private val readTweets: ReadTweets) : ViewModel() {
    private var mutableTweetsListLive: MutableLiveData<List<String>> = MutableLiveData()
    var tweetsListLive: LiveData<List<String>> = mutableTweetsListLive
    private var mutableErrorLive: MutableLiveData<Throwable> = MutableLiveData()
    var errorLive: LiveData<Throwable> = mutableErrorLive


    fun onReadTweets(nickname: String) {
        viewModelScope.launch {
            kotlin.runCatching {
                readTweets(nickname)
            }.onSuccess {
                mutableTweetsListLive.value = it
            }.onFailure {
                mutableErrorLive.value = it
            }
        }
    }
}
