package com.nayla.twitter_app.presentation.following

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nayla.twitter_app.core.action.GetFollowing
import com.nayla.twitter_app.infrastructure.ApiRestUserServiceFactory

class FollowingViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val getFollowing = GetFollowing(ApiRestUserServiceFactory.getService())
        return FollowingViewModel(getFollowing) as T
    }
}
