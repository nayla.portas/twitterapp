package com.nayla.twitter_app.presentation.tweet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nayla.twitter_app.core.action.SendTweet
import kotlinx.coroutines.launch

class SendTweetViewModel(private val sendTweet: SendTweet) : ViewModel() {
    private var mutableErrorLive: MutableLiveData<Throwable> = MutableLiveData()
    var errorLive: LiveData<Throwable> = mutableErrorLive
    private var mutableOkLive: MutableLiveData<Boolean> = MutableLiveData()
    var okLive: LiveData<Boolean> = mutableOkLive

    fun onSendTweet(nickname: String, text: String) {
        viewModelScope.launch {
            kotlin.runCatching {
                sendTweet(nickname, text)
            }.onSuccess {
                mutableOkLive.value = true
            }.onFailure {
                mutableErrorLive.value = it
            }
        }
    }
}
