package com.nayla.twitter_app.presentation.follow

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nayla.twitter_app.R
import com.nayla.twitter_app.infrastructure.TwitterException

class FollowActivity : AppCompatActivity() {
    private lateinit var followViewModel: FollowViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_follow)
        initViewModel()
        bindFollowButton()
        observeFollowOk()
        observeFollowErrors()
    }

    private fun observeFollowErrors() {
        followViewModel.errorLive.observe(this, Observer {
            var message = when (it) {
                is TwitterException.TwitterFollowException -> R.string.userDoesNotExist
                is TwitterException.FieldEmptyException -> R.string.nicknameCantBeEmpty
                else -> R.string.cantFollow
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        })
    }

    private fun observeFollowOk() {
        followViewModel.okLive.observe(this, Observer {
            Toast.makeText(this, R.string.followOk, Toast.LENGTH_SHORT).show()
        })
    }

    private fun bindFollowButton() {
        val buttonFollow = findViewById<Button>(R.id.followSubmitButton)

        buttonFollow.setOnClickListener {
            val nickname = findViewById<TextView>(R.id.followNicknameText).text.toString()
            val nicknameToFollow =
                findViewById<TextView>(R.id.followNicknameToFollowText).text.toString()
            followViewModel.onFollowUser(nickname, nicknameToFollow)
        }
    }

    private fun initViewModel() {
        followViewModel =
            ViewModelProvider(this, FollowViewModelFactory())[FollowViewModel::class.java]
    }
}
