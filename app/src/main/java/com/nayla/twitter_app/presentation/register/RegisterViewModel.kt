package com.nayla.twitter_app.presentation.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nayla.twitter_app.core.action.RegisterUser
import kotlinx.coroutines.launch

class RegisterViewModel(private val registerUser: RegisterUser) : ViewModel() {
    private var mutableErrorLive: MutableLiveData<Throwable> = MutableLiveData()
    var errorLive: LiveData<Throwable> = mutableErrorLive
    private var mutableOkLive: MutableLiveData<Boolean> = MutableLiveData()
    var okLive: LiveData<Boolean> = mutableOkLive

    fun onRegisterUser(nickname: String, name: String) {
        viewModelScope.launch {
            kotlin.runCatching {
                registerUser(nickname, name)
            }.onSuccess {
                mutableOkLive.value = true
            }.onFailure {
                mutableErrorLive.value = it
            }
        }
    }
}
