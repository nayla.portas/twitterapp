package com.nayla.twitter_app.presentation.follow

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nayla.twitter_app.core.action.FollowUser
import kotlinx.coroutines.launch

class FollowViewModel(private val followUser: FollowUser) : ViewModel() {
    private var mutableErrorLive: MutableLiveData<Throwable> = MutableLiveData()
    var errorLive: LiveData<Throwable> = mutableErrorLive
    private var mutableOkLive: MutableLiveData<Boolean> = MutableLiveData()
    var okLive: LiveData<Boolean> = mutableOkLive

    fun onFollowUser(nickname: String, nicknameToFollow: String) {
        viewModelScope.launch {
            kotlin.runCatching {
                followUser(nickname, nicknameToFollow)
            }.onSuccess {
                mutableOkLive.value = true
            }.onFailure {
                mutableErrorLive.value = it
            }
        }
    }
}
