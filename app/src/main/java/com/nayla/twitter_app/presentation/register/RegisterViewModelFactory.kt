package com.nayla.twitter_app.presentation.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nayla.twitter_app.core.action.RegisterUser
import com.nayla.twitter_app.infrastructure.ApiRestUserServiceFactory

class RegisterViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val registerUser = RegisterUser(ApiRestUserServiceFactory.getService())
        return RegisterViewModel(registerUser) as T
    }
}
