package com.nayla.twitter_app.presentation.following

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nayla.twitter_app.core.action.GetFollowing
import kotlinx.coroutines.launch

class FollowingViewModel(private val getFollowing: GetFollowing) : ViewModel() {
    var followingListLive: MutableLiveData<List<String>> = MutableLiveData()
    private var mutableErrorLive: MutableLiveData<Throwable> = MutableLiveData()
    var errorLive: LiveData<Throwable> = mutableErrorLive

    fun onGetFollowing(nickname: String) {
        viewModelScope.launch {
            kotlin.runCatching {
                getFollowing(nickname)
            }.onSuccess {
                followingListLive.value = it
            }.onFailure {
                mutableErrorLive.value = it
            }
        }
    }
}
