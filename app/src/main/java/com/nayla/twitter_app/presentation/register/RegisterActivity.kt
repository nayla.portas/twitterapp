package com.nayla.twitter_app.presentation.register

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nayla.twitter_app.R
import com.nayla.twitter_app.infrastructure.TwitterException

class RegisterActivity : AppCompatActivity() {
    private lateinit var registerViewModel : RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initViewModel()
        bindRegisterButton()
        observeRegisterOk()
        observeRegisterErrors()
    }

    private fun initViewModel() {
        registerViewModel =
            ViewModelProvider(this, RegisterViewModelFactory())[RegisterViewModel::class.java]
    }

    private fun bindRegisterButton() {
        val buttonRegister = findViewById<Button>(R.id.registerSubmitButton)

        buttonRegister.setOnClickListener {
            val nickname = findViewById<TextView>(R.id.registerNicknameText).text.toString()
            val name = findViewById<TextView>(R.id.registerNameText).text.toString()
            registerViewModel.onRegisterUser(nickname, name)
        }
    }

    private fun observeRegisterOk() {
        registerViewModel.okLive.observe(this, Observer {
            Toast.makeText(this, R.string.registerOk, Toast.LENGTH_SHORT).show()
        })
    }

    private fun observeRegisterErrors() {
        registerViewModel.errorLive.observe(this, Observer {
            var message = when (it) {
                is TwitterException.TwitterRegisterException -> R.string.userAlreadyExists
                is TwitterException.FieldEmptyException -> R.string.nameOrNicknameCantBeEmpty
                else -> R.string.cantRegisterUser
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        })
    }
}
