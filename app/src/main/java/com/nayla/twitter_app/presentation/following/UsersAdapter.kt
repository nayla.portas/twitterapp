package com.nayla.twitter_app.presentation.following

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UsersAdapter(private var users: List<String>) : RecyclerView.Adapter<MyViewHolder>() {

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(parent)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(users[position])
    }

    fun updateList(users: List<String>) {
        this.users = users
        notifyDataSetChanged()
    }
}

class MyViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_1, parent, false)
) {
    private val textView = itemView.findViewById<TextView>(android.R.id.text1)

    fun bind(text: String) {
        textView.text = text
    }
}
