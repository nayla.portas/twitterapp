package com.nayla.twitter_app.presentation.tweet

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nayla.twitter_app.core.action.SendTweet
import com.nayla.twitter_app.infrastructure.ApiRestUserServiceFactory

class SendTweetViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val sendTweet = SendTweet(ApiRestUserServiceFactory.getService())
        return SendTweetViewModel(sendTweet) as T
    }
}
