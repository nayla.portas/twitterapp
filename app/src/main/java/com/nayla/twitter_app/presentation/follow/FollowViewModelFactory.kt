package com.nayla.twitter_app.presentation.follow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nayla.twitter_app.core.action.FollowUser
import com.nayla.twitter_app.infrastructure.ApiRestUserServiceFactory

class FollowViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val followUser = FollowUser(ApiRestUserServiceFactory.getService())
        return FollowViewModel(followUser) as T
    }
}
