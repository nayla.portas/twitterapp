package com.nayla.twitter_app.presentation.following

import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nayla.twitter_app.R
import com.nayla.twitter_app.infrastructure.TwitterException

class FollowingActivity : AppCompatActivity() {

    private lateinit var followingViewModel : FollowingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_following)
        initViewModel()
        bindGetFollowingButton()
        bindListView()
        observeGetFollowingErrors()
    }

    private fun initViewModel() {
        followingViewModel =
            ViewModelProvider(this, FollowingViewModelFactory())[FollowingViewModel::class.java]
    }

    private fun bindGetFollowingButton() {
        val buttonGetFollowing = findViewById<Button>(R.id.followingSubmitButton)

        buttonGetFollowing.setOnClickListener {
            val nickname = findViewById<TextView>(R.id.followingNicknameText).text.toString()
            followingViewModel.onGetFollowing(nickname)
        }
    }

    private fun bindListView() {
        val adapter = UsersAdapter(emptyList())
        val usersView = findViewById<RecyclerView>(R.id.followingListView)
        usersView.adapter = adapter
        usersView.layoutManager = LinearLayoutManager(this)

        followingViewModel.followingListLive.observe(this) {
            adapter.updateList(it)
        }
    }

    private fun observeGetFollowingErrors() {
        followingViewModel.errorLive.observe(this) {
            val message = when (it) {
                is TwitterException.TwitterGetFollowingException -> R.string.userDoesNotExist
                else -> R.string.cantGetFollowing
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }
}
