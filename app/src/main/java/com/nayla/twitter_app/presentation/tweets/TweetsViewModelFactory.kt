package com.nayla.twitter_app.presentation.tweets

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nayla.twitter_app.core.action.ReadTweets
import com.nayla.twitter_app.infrastructure.ApiRestUserServiceFactory

class TweetsViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val readTweets = ReadTweets(ApiRestUserServiceFactory.getService())
        return TweetsViewModel(readTweets) as T
    }
}
