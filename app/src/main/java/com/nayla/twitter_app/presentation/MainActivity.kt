package com.nayla.twitter_app.presentation


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.nayla.twitter_app.R
import com.nayla.twitter_app.presentation.follow.FollowActivity
import com.nayla.twitter_app.presentation.following.FollowingActivity
import com.nayla.twitter_app.presentation.register.RegisterActivity
import com.nayla.twitter_app.presentation.tweet.SendTweetActivity
import com.nayla.twitter_app.presentation.tweets.TweetsActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bindRegisterButton()
        bindTweetButton()
        bindTweetsButton()
        bindFollowButton()
        bindFollowingButton()
    }

    private fun bindRegisterButton() {
        val buttonRegister = findViewById<Button>(R.id.buttonRegister)

        buttonRegister.setOnClickListener {
            val activityIntent = Intent(this, RegisterActivity::class.java)
            startActivity(activityIntent)
        }
    }

    private fun bindTweetButton() {
        val buttonTweet = findViewById<Button>(R.id.buttonTweet)

        buttonTweet.setOnClickListener {
            val activityIntent = Intent(this, SendTweetActivity::class.java)
            startActivity(activityIntent)
        }
    }

    private fun bindTweetsButton() {
        val buttonTweets = findViewById<Button>(R.id.buttonTweets)

        buttonTweets.setOnClickListener {
            val activityIntent = Intent(this, TweetsActivity::class.java)
            startActivity(activityIntent)
        }
    }

    private fun bindFollowButton() {
        val buttonFollow = findViewById<Button>(R.id.buttonFollow)

        buttonFollow.setOnClickListener {
            val activityIntent = Intent(this, FollowActivity::class.java)
            startActivity(activityIntent)
        }
    }

    private fun bindFollowingButton() {
        val buttonFollowing = findViewById<Button>(R.id.buttonFollowing)

        buttonFollowing.setOnClickListener {
            val activityIntent = Intent(this, FollowingActivity::class.java)
            startActivity(activityIntent)
        }
    }
}
