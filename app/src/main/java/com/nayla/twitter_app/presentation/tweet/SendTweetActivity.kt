package com.nayla.twitter_app.presentation.tweet

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nayla.twitter_app.R
import com.nayla.twitter_app.infrastructure.TwitterException

class SendTweetActivity : AppCompatActivity() {
    private lateinit var sendTweetViewModel : SendTweetViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_tweet)
        initViewModel()
        bindSendTweetButton()
        observeSendTweetOk()
        observeSendTweetErrors()
    }

    private fun initViewModel() {
        sendTweetViewModel =
            ViewModelProvider(this, SendTweetViewModelFactory())[SendTweetViewModel::class.java]
    }

    private fun bindSendTweetButton() {
        val buttonSendTweet = findViewById<Button>(R.id.tweetSubmitButton)

        buttonSendTweet.setOnClickListener {
            val nickname = findViewById<TextView>(R.id.tweetNicknameText).text.toString()
            val text = findViewById<TextView>(R.id.tweetTextText).text.toString()
            sendTweetViewModel.onSendTweet(nickname, text)
        }
    }

    private fun observeSendTweetOk() {
        sendTweetViewModel.okLive.observe(this, Observer {
            Toast.makeText(this, R.string.tweetOk, Toast.LENGTH_SHORT).show()
        })
    }

    private fun observeSendTweetErrors() {
        sendTweetViewModel.errorLive.observe(this, Observer {
            var message = when (it) {
                is TwitterException.TwitterSendTweetException -> R.string.userDoesNotExist
                is TwitterException.FieldEmptyException -> R.string.nicknameOrTweetCantBeEmpty
                else -> R.string.cantSendTweet
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        })
    }
}
