package com.nayla.twitter_app.presentation.tweets

import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nayla.twitter_app.R
import com.nayla.twitter_app.infrastructure.TwitterException

class TweetsActivity : AppCompatActivity() {
    private lateinit var tweetsViewModel : TweetsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tweets)
        initViewModel()
        bindReadTweetsButton()
        bindListView()
        observeReadTweetsErrors()
    }

    private fun initViewModel() {
        tweetsViewModel =
            ViewModelProvider(this, TweetsViewModelFactory())[TweetsViewModel::class.java]
    }

    private fun bindReadTweetsButton() {
        val buttonReadTweets = findViewById<Button>(R.id.tweetsSubmitButton)

        buttonReadTweets.setOnClickListener {
            val nickname = findViewById<TextView>(R.id.tweetsNicknameText).text.toString()
            tweetsViewModel.onReadTweets(nickname)
        }
    }

    private fun bindListView() {
        val adapter = TweetsAdapter(emptyList())
        val tweetsView = findViewById<RecyclerView>(R.id.tweetsListView)
        tweetsView.adapter = adapter
        tweetsView.layoutManager = LinearLayoutManager(this)

        tweetsViewModel.tweetsListLive.observe(this, Observer {
            adapter.updateList(it)
        })
    }

    private fun observeReadTweetsErrors() {
        tweetsViewModel.errorLive.observe(this, Observer {
            var message = when (it) {
                is TwitterException.TwitterReadTweetsException -> R.string.userDoesNotExist
                else -> R.string.cantReadTweets
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        })
    }
}
