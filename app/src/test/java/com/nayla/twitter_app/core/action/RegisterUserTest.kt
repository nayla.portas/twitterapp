package com.nayla.twitter_app.core.action

import com.nayla.twitter_app.core.domain.UserData
import com.nayla.twitter_app.core.repository.UserService
import com.nayla.twitter_app.infrastructure.TwitterException
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test


internal class RegisterUserTest {
    private val userService = mockk<UserService>(relaxed = true)
    private lateinit var registerUser: RegisterUser
    private var exception: Throwable? = null

    @Before
    fun setup() {
        registerUser = RegisterUser(userService)
    }

    @Test
    fun `should invoke add user`() = runTest() {
        whenRegisterUser(NICKNAME, NAME)
        thenAddUserIsInvoked()
    }

    @Test
    fun `should throw an error when name is empty`() = runTest() {
        whenRegisterUser("", NICKNAME)
        thenRegisterUserThrowsAnException(EMPTY_FIELD_ERROR)
    }

    @Test
    fun `should throw an error when nickname is empty`() = runTest() {
        whenRegisterUser(NICKNAME, "")
        thenRegisterUserThrowsAnException(EMPTY_FIELD_ERROR)
    }

    private suspend fun whenRegisterUser(name: String, nickname: String) {
        try {
            registerUser.invoke(name, nickname)
        } catch(e: Throwable) {
            exception = e
        }
    }

    private fun thenAddUserIsInvoked() {
        coVerify { userService.add(USER) }
    }

    private fun thenRegisterUserThrowsAnException(error: TwitterException) {
        Assertions.assertThat(exception).isInstanceOf(error::class.java)
    }

    private companion object {
        const val NICKNAME = "denu"
        const val NAME = "denisse"
        var USER = UserData(NICKNAME, NAME)
        val EMPTY_FIELD_ERROR = TwitterException.FieldEmptyException()
    }
}
