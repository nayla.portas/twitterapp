package com.nayla.twitter_app.core.action

import com.nayla.twitter_app.core.repository.UserService
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test


internal class GetFollowingTest {
    private val userService = mockk<UserService>(relaxed = true)
    private lateinit var getFollowing: GetFollowing
    private lateinit var following: List<String>

    @Before
    fun setup() {
        getFollowing = GetFollowing(userService)
    }

    @Test
    fun `should invoke following`() = runTest() {
        givenAUserHasFollowing()
        whenGetFollowing(NICKNAME)
        thenFollowingIsInvoked(NICKNAME)
    }

    private fun givenAUserHasFollowing() {
        coEvery { userService.following(NICKNAME) } returns listOf(NICKNAME_TO_FOLLOW)
    }

    private suspend fun whenGetFollowing(nickname: String) {
        following = getFollowing.invoke(nickname)
    }

    private fun thenFollowingIsInvoked(nickname: String) {
        coVerify { userService.following(nickname) }
        assertThat(following).hasSize(1)
    }

    private companion object {
        const val NICKNAME = "nick"
        const val NICKNAME_TO_FOLLOW = "denu"
    }
}
