package com.nayla.twitter_app.core.action

import com.nayla.twitter_app.core.domain.TweetData
import com.nayla.twitter_app.core.repository.UserService
import com.nayla.twitter_app.infrastructure.TwitterException
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test


internal class SendTweetTest {
    private val userService = mockk<UserService>(relaxed = true)
    private lateinit var sendTweet: SendTweet
    private var exception: Throwable? = null

    @Before
    fun setup() {
        sendTweet = SendTweet(userService)
    }

    @Test
    fun `should invoke send tweet`() = runTest() {
        whenSendTweet(NICKNAME, TEXT)
        thenTweetIsInvoked()
    }

    @Test
    fun `should throw an error when nickname is empty`() = runTest() {
        whenSendTweet("", TEXT)
        thenSendTweetThrowsAnException(EMPTY_FIELD_ERROR)
    }

    @Test
    fun `should throw an error when tweet is empty`() = runTest() {
        whenSendTweet(NICKNAME, "")
        thenSendTweetThrowsAnException(EMPTY_FIELD_ERROR)
    }

    private suspend fun whenSendTweet(nickname: String, tweet: String) {
        try {
            sendTweet.invoke(nickname, tweet)
        } catch(e: Throwable) {
            exception = e
        }
    }

    private fun thenTweetIsInvoked() {
        coVerify { userService.sendTweet(TWEET) }
    }

    private fun thenSendTweetThrowsAnException(error: TwitterException) {
        Assertions.assertThat(exception).isInstanceOf(error::class.java)
    }

    private companion object {
        const val NICKNAME = "denu"
        const val TEXT = "hola mundo!"
        var TWEET = TweetData(NICKNAME, TEXT)
        val EMPTY_FIELD_ERROR = TwitterException.FieldEmptyException()
    }
}
