package com.nayla.twitter_app.core.action


import com.nayla.twitter_app.core.domain.FollowData
import com.nayla.twitter_app.core.repository.UserService
import com.nayla.twitter_app.infrastructure.TwitterException
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test


internal class FollowUserTest {
    private val userService = mockk<UserService>(relaxed = true)
    private lateinit var followUser: FollowUser
    private var exception: Throwable? = null

    @Before
    fun setup() {
        followUser = FollowUser(userService)
    }

    @Test
    fun `should invoke follow user`() = runTest() {
        whenFollowUser(NICKNAME, NICKNAME_TO_FOLLOW)
        thenFollowUserIsInvoked()
    }

    @Test
    fun `should throw an error when nickname is empty`() = runTest() {
        whenFollowUser("", NICKNAME_TO_FOLLOW)
        thenFollowUserThrowsAnException(EMPTY_FIELD_ERROR)
    }

    @Test
    fun `should throw an error when nickname to follow is empty`() = runTest() {
        whenFollowUser(NICKNAME, "")
        thenFollowUserThrowsAnException(EMPTY_FIELD_ERROR)
    }

    private suspend fun whenFollowUser(nickname: String, nicknameToFollow: String) {
        try {
            followUser.invoke(nickname, nicknameToFollow)
        } catch(e: Throwable) {
            exception = e
        }
    }

    private fun thenFollowUserIsInvoked() {
        coVerify { userService.followUser(FOLLOW) }
    }

    private fun thenFollowUserThrowsAnException(error: TwitterException) {
        Assertions.assertThat(exception).isInstanceOf(error::class.java)
    }

    private companion object {
        const val NICKNAME = "denu"
        const val NICKNAME_TO_FOLLOW = "nick"
        var FOLLOW = FollowData(NICKNAME, NICKNAME_TO_FOLLOW)
        val EMPTY_FIELD_ERROR = TwitterException.FieldEmptyException()
    }
}
