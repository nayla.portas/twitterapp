package com.nayla.twitter_app.core.action

import com.nayla.twitter_app.core.repository.UserService
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test


internal class ReadTweetsTest {
    private val userService = mockk<UserService>(relaxed = true)
    private lateinit var readTweets: ReadTweets
    private lateinit var tweets: List<String>

    @Before
    fun setup() {
        readTweets = ReadTweets(userService)
    }

    @Test
    fun `should invoke tweets`() = runTest() {
        givenAUserHasTweets()
        whenReadTweets(NICKNAME)
        thenTweetsIsInvoked(NICKNAME)
    }

    private fun givenAUserHasTweets() {
        coEvery { userService.tweets(NICKNAME) } returns listOf(TWEET)
    }

    private suspend fun whenReadTweets(nickname: String) {
        tweets = readTweets.invoke(nickname)
    }

    private fun thenTweetsIsInvoked(nickname: String) {
        coVerify { userService.tweets(nickname) }
        Assertions.assertThat(tweets).hasSize(1)
    }

    private companion object {
        const val NICKNAME = "nick"
        const val TWEET =  "hola mundo!"
    }
}
