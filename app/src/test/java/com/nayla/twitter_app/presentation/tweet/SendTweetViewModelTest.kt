package com.nayla.twitter_app.presentation.tweet

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nayla.twitter_app.core.action.SendTweet
import com.nayla.twitter_app.infrastructure.TwitterException
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
internal class SendTweetViewModelTest {
    private val sendTweet = mockk<SendTweet>(relaxed = true)
    private lateinit var sendTweetViewModel: SendTweetViewModel
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        sendTweetViewModel = SendTweetViewModel(sendTweet)
    }

    @After
    fun finished() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should invoke send tweet`() = runTest() {
        whenSendTweet(NICKNAME, TWEET)
        thenSendTweetIsInvoked(NICKNAME, TWEET)
    }

    @Test
    fun `should throw an error when send a tweet`() = runTest() {
        givenTheUserIsInvalid(SEND_TWEET_ERROR)
        whenSendTweet(NICKNAME, TWEET)
        thenSendTweetThrowsAnException(SEND_TWEET_ERROR)
    }

    @Test
    fun `should send a tweet successfully`() = runTest() {
        whenSendTweet(NICKNAME, TWEET)
        thenTheTweetIsSentSuccessfully()
    }

    private fun givenTheUserIsInvalid(error: TwitterException) {
        coEvery { sendTweet.invoke(any(), any()) } throws error
    }

    private fun whenSendTweet(nickname: String, tweet: String) {
        sendTweetViewModel.onSendTweet(nickname, tweet)
    }

    private fun thenSendTweetIsInvoked(nickname: String, tweet: String) {
        coVerify { sendTweet.invoke(nickname, tweet) }
    }

    private fun thenSendTweetThrowsAnException(error: TwitterException) {
        assertThat(sendTweetViewModel.errorLive.value).isInstanceOf(error::class.java)
    }

    private fun thenTheTweetIsSentSuccessfully() {
        assertThat(sendTweetViewModel.okLive.value).isTrue
    }

    private companion object {
        const val NICKNAME = "nick"
        const val TWEET = "hola mundo!"
        val SEND_TWEET_ERROR = TwitterException.TwitterSendTweetException()
    }
}
