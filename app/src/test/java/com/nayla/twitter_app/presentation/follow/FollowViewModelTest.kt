package com.nayla.twitter_app.presentation.follow

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nayla.twitter_app.core.action.FollowUser
import com.nayla.twitter_app.infrastructure.TwitterException
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
internal class FollowViewModelTest {
    private val followUser = mockk<FollowUser>(relaxed = true)
    private lateinit var followViewModel: FollowViewModel
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        followViewModel = FollowViewModel(followUser)
    }

    @After
    fun finished() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should invoke follow user`() = runTest() {
        whenFollowUser(NICKNAME, NICKNAME_TO_FOLLOW)
        thenFollowUserIsInvoked(NICKNAME, NICKNAME_TO_FOLLOW)
    }

    @Test
    fun `should throw an error when follow a user`() = runTest() {
        givenTheUserIsInvalid(FOLLOW_ERROR)
        whenFollowUser(NICKNAME, NICKNAME_TO_FOLLOW)
        thenFollowUserThrowsAnException(FOLLOW_ERROR)
    }

    @Test
    fun `should follow an user successfully`() = runTest() {
        whenFollowUser(NICKNAME, NICKNAME_TO_FOLLOW)
        thenTheUserIsFollowedSuccessfully()
    }

    private fun givenTheUserIsInvalid(error: TwitterException) {
        coEvery { followUser.invoke(any(), any()) } throws error
    }

    private fun whenFollowUser(nickname: String, nicknameToFollow: String) {
        followViewModel.onFollowUser(nickname, nicknameToFollow)
    }

    private fun thenFollowUserIsInvoked(nickname: String, nicknameToFollow: String) {
        coVerify { followUser.invoke(nickname, nicknameToFollow) }
    }

    private fun thenFollowUserThrowsAnException(error: TwitterException) {
        assertThat(followViewModel.errorLive.value).isInstanceOf(error::class.java)
    }

    private fun thenTheUserIsFollowedSuccessfully() {
        assertThat(followViewModel.okLive.value).isTrue
    }

    private companion object {
        const val NICKNAME = "nick"
        const val NICKNAME_TO_FOLLOW = "nay"
        val FOLLOW_ERROR = TwitterException.TwitterFollowException()
    }
}
