package com.nayla.twitter_app.presentation.following

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nayla.twitter_app.core.action.GetFollowing
import com.nayla.twitter_app.infrastructure.TwitterException
import com.nayla.twitter_app.presentation.register.RegisterViewModelTest
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
internal class FollowingViewModelTest {
    private val getFollowing = mockk<GetFollowing>(relaxed = true)
    private lateinit var followingViewModel: FollowingViewModel
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        followingViewModel = FollowingViewModel(getFollowing)
    }

    @After
    fun finished() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should invoke get following`() = runTest() {
        whenGetFollowing(NICKNAME)
        thenGetFollowingIsInvoked(NICKNAME)
    }

    @Test
    fun `should throw an error when get following`() = runTest() {
        givenTheUserIsInvalid(FOLLOWING_ERROR)
        whenGetFollowing(NICKNAME)
        thenGetFollowingThrowsAnException(FOLLOWING_ERROR)
    }

    @Test
    fun `should get following successfully`() = runTest() {
        givenAUserHasFollowing()
        whenGetFollowing(NICKNAME)
        thenGetFollowingSuccessfully()
    }

    private fun givenAUserHasFollowing() {
        coEvery { getFollowing.invoke(NICKNAME) } returns FOLLOWING
    }

    private fun givenTheUserIsInvalid(error: TwitterException) {
        coEvery { getFollowing.invoke(any()) } throws error
    }

    private fun whenGetFollowing(nickname: String) {
        followingViewModel.onGetFollowing(nickname)
    }

    private fun thenGetFollowingIsInvoked(nickname: String) {
        coVerify { getFollowing.invoke(nickname) }
    }

    private fun thenGetFollowingThrowsAnException(error: TwitterException) {
        assertThat(followingViewModel.errorLive.value).isInstanceOf(error::class.java)
    }

    private fun thenGetFollowingSuccessfully() {
        assertThat(followingViewModel.followingListLive.value).hasSize(1)
        assertThat(followingViewModel.followingListLive.value).isEqualTo(FOLLOWING)
    }

    private companion object {
        const val NICKNAME = "nick"
        const val NICKNAME_TO_FOLLOW = "nay"
        val FOLLOWING_ERROR = TwitterException.TwitterGetFollowingException()
        val FOLLOWING = listOf(NICKNAME_TO_FOLLOW)
    }
}