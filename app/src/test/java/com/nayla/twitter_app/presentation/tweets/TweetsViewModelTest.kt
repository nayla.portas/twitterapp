package com.nayla.twitter_app.presentation.tweets

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nayla.twitter_app.core.action.GetFollowing
import com.nayla.twitter_app.core.action.ReadTweets
import com.nayla.twitter_app.infrastructure.TwitterException
import com.nayla.twitter_app.presentation.following.FollowingViewModel
import com.nayla.twitter_app.presentation.following.FollowingViewModelTest
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
internal class TweetsViewModelTest {
    private val readTweets = mockk<ReadTweets>(relaxed = true)
    private lateinit var tweetsViewModel: TweetsViewModel
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        tweetsViewModel = TweetsViewModel(readTweets)
    }

    @After
    fun finished() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should invoke get following`() = runTest() {
        whenReadTweets(NICKNAME)
        thenReadTweetsIsInvoked(NICKNAME)
    }

    @Test
    fun `should throw an error when read tweets`() = runTest() {
        givenTheUserIsInvalid(READ_TWEETS_ERROR)
        whenReadTweets(NICKNAME)
        thenReadTweetsThrowsAnException(READ_TWEETS_ERROR)
    }

    @Test
    fun `should read tweets successfully`() = runTest() {
        givenAUserHasTweets()
        whenReadTweets(NICKNAME)
        thenReadTweetsSuccessfully()
    }

    private fun givenTheUserIsInvalid(error: TwitterException) {
        coEvery { readTweets.invoke(any()) } throws error
    }

    private fun givenAUserHasTweets() {
        coEvery { readTweets.invoke(NICKNAME) } returns TWEETS
    }

    private fun whenReadTweets(nickname: String) {
        tweetsViewModel.onReadTweets(nickname)
    }

    private fun thenReadTweetsIsInvoked(nickname: String) {
        coVerify { readTweets.invoke(nickname) }
    }

    private fun thenReadTweetsThrowsAnException(error: TwitterException) {
         assertThat(tweetsViewModel.errorLive.value).isInstanceOf(error::class.java)
    }

    private fun thenReadTweetsSuccessfully() {
        assertThat(tweetsViewModel.tweetsListLive.value).hasSize(1)
        assertThat(tweetsViewModel.tweetsListLive.value).isEqualTo(TWEETS)
    }

    private companion object {
        const val NICKNAME = "nick"
        const val TWEET = "hola mundo!"
        val READ_TWEETS_ERROR = TwitterException.TwitterReadTweetsException()
        val TWEETS = listOf(TWEET)
    }
}
