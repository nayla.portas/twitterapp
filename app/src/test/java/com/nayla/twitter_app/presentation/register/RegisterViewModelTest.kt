package com.nayla.twitter_app.presentation.register

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nayla.twitter_app.core.action.RegisterUser
import com.nayla.twitter_app.infrastructure.TwitterException
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
internal class RegisterViewModelTest {
    private val registerUser = mockk<RegisterUser>(relaxed = true)
    private lateinit var registerViewModel : RegisterViewModel
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        registerViewModel = RegisterViewModel(registerUser)
    }

    @After
    fun finished() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should invoke register user`() = runTest() {
        whenRegisterUser(NICKNAME, NAME)
        thenRegisterUserIsInvoked(NICKNAME, NAME)
    }

    @Test
    fun `should throw an error when register user`() = runTest() {
        givenTheUserIsInvalid(REGISTER_ERROR)
        whenRegisterUser(NICKNAME, NAME)
        thenRegisterUserThrowsAnException(REGISTER_ERROR)
    }

    @Test
    fun `should register an user successfully`() = runTest() {
        whenRegisterUser(NICKNAME, NAME)
        thenTheUserIsRegisteredSuccessfully()
    }

    private fun givenTheUserIsInvalid(error: TwitterException) {
        coEvery { registerUser.invoke(any(), any()) } throws error
    }

    private fun whenRegisterUser(nickname: String, name: String) {
        registerViewModel.onRegisterUser(nickname, name)
    }

    private fun thenRegisterUserIsInvoked(nickname: String, name: String) {
        coVerify { registerUser.invoke(nickname, name) }
    }

    private fun thenRegisterUserThrowsAnException(error: TwitterException) {
        assertThat(registerViewModel.errorLive.value).isInstanceOf(error::class.java)
    }

    private fun thenTheUserIsRegisteredSuccessfully() {
        assertThat(registerViewModel.okLive.value).isTrue
    }

    private companion object {
        const val NICKNAME = "nick"
        const val NAME = "nicolas"
        val REGISTER_ERROR = TwitterException.TwitterRegisterException()
    }
}
