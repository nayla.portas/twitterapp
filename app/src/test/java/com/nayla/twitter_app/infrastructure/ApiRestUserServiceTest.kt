package com.nayla.twitter_app.infrastructure

import com.nayla.twitter_app.core.domain.FollowData
import com.nayla.twitter_app.core.domain.TweetData
import com.nayla.twitter_app.core.domain.UserData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
internal class ApiRestUserServiceTest {
    private val apiRestClient = mockk<ApiRestClient>(relaxed = true)
    private lateinit var apiRestUserService: ApiRestUserService
    private lateinit var following: List<String>
    private lateinit var tweets: List<String>
    private var exception: Throwable? = null

    @Before
    fun setup() {
        apiRestUserService = ApiRestUserService(apiRestClient)
    }

    @Test
    fun `should invoke register user`() = runTest() {
        whenAddAUser(USER)
        thenRegisterUserIsInvoked(USER)
    }

    @Test
    fun `should invoke send tweet`() = runTest() {
        whenSendTweet(TWEET)
        thenSendTweetIsInvoked(TWEET)
    }

    @Test
    fun `should invoke read tweets`() = runTest() {
        givenAUserHasTweets(NICKNAME, TWEETS)
        whenReadTweets(NICKNAME)
        thenReadTweetsIsInvoked(NICKNAME, TWEETS)
    }

    @Test
    fun `should invoke follow user`() = runTest() {
        whenFollowUser(FOLLOW)
        thenFollowUserIsInvoked(FOLLOW)
    }

    @Test
    fun `should invoke following`() = runTest() {
        givenAUserHasFollowing(NICKNAME, FOLLOWING)
        whenGetFollowingUser(NICKNAME)
        thenFollowingIsInvoked(NICKNAME, FOLLOWING)
    }

    @Test
    fun `should throws an TwitterRegisterException when the user already exists`() = runTest() {
        givenTheUserAlreadyExists(USER)
        whenAddAUser(USER)
        thenThrowsAnException(TwitterException.TwitterRegisterException())
    }

    @Test
    fun `should throws an TwitterSendTweetException when the user does not exist`() = runTest() {
        givenTheUserDoesNotExist()
        whenSendTweet(TWEET)
        thenThrowsAnException(TwitterException.TwitterSendTweetException())
    }

    @Test
    fun `should throws an TwitterReadTweetsException when the user does not exist`() = runTest() {
        givenTheUserDoesNotExist()
        whenReadTweets(NICKNAME)
        thenThrowsAnException(TwitterException.TwitterReadTweetsException())
    }

    @Test
    fun `should throws an TwitterFollowException when the user does not exist`() = runTest() {
        givenTheUserDoesNotExist()
        whenFollowUser(FOLLOW)
        thenThrowsAnException(TwitterException.TwitterFollowException())
    }

    @Test
    fun `should throws an TwitterGetFollowingException when the user does not exist`() = runTest() {
        givenTheUserDoesNotExist()
        whenGetFollowingUser(NICKNAME)
        thenThrowsAnException(TwitterException.TwitterGetFollowingException())
    }

    private fun givenAUserHasTweets(nickname: String, tweets: List<String>) {
        coEvery { apiRestClient.readTweets(nickname) } returns tweets
    }

    private fun givenAUserHasFollowing(nickname: String, following: List<String>) {
        coEvery { apiRestClient.following(nickname) } returns following
    }

    private fun givenTheUserAlreadyExists(user: UserData) {
        coEvery { apiRestClient.registerUser(user) } throws Throwable()
    }

    private fun givenTheUserDoesNotExist() {
        coEvery { apiRestClient.sendTweet(any()) } throws Throwable()
        coEvery { apiRestClient.readTweets(any()) } throws Throwable()
        coEvery { apiRestClient.followUser(any()) } throws Throwable()
        coEvery { apiRestClient.following(any()) } throws Throwable()
    }

    private suspend fun whenAddAUser(user: UserData) {
        try {
            apiRestUserService.add(user)
        } catch (e: Throwable) {
            exception = e
        }
    }

    private suspend fun whenSendTweet(tweet: TweetData) {
        try {
            apiRestUserService.sendTweet(tweet)
        } catch(e: Throwable) {
            exception = e
        }
    }

    private suspend fun whenReadTweets(nickname: String) {
        try {
            tweets = apiRestUserService.tweets(nickname)
        } catch (e: Throwable) {
            exception = e
        }
    }

    private suspend fun whenFollowUser(follow: FollowData) {
        try {
            apiRestUserService.followUser(follow)
        } catch (e: Throwable) {
            exception = e
        }
    }

    private suspend fun whenGetFollowingUser(nickname: String) {
        try {
            following = apiRestUserService.following(nickname)
        } catch (e: Throwable) {
            exception = e
        }
    }

    private fun thenRegisterUserIsInvoked(user: UserData) {
        coVerify { apiRestClient.registerUser(user) }
    }

    private fun thenSendTweetIsInvoked(tweet: TweetData) {
        coVerify { apiRestClient.sendTweet(tweet) }
    }

    private fun thenReadTweetsIsInvoked(nickname: String, expectedTweets: List<String>) {
        coVerify { apiRestClient.readTweets(nickname) }
        assertThat(tweets).hasSize(1)
        assertThat(tweets).isEqualTo(expectedTweets)
    }

    private fun thenFollowUserIsInvoked(follow: FollowData) {
        coVerify { apiRestClient.followUser(follow) }
    }

    private fun thenFollowingIsInvoked(nickname: String, expectedFollowing: List<String>) {
        coVerify { apiRestClient.following(nickname) }
        assertThat(following).hasSize(1)
        assertThat(following).isEqualTo(expectedFollowing)
    }

    private fun thenThrowsAnException(error: TwitterException) {
        assertThat(exception).isNotNull
        assertThat(exception).isInstanceOf(error::class.java)
    }

    private companion object {
        const val NICKNAME = "nick"
        const val NAME = "nicolas"
        const val TEXT = "hola mundo!"
        const val NICKNAME_TO_FOLLOW = "denu"
        val USER = UserData(NICKNAME, NAME)
        val TWEET = TweetData(NICKNAME, TEXT)
        val FOLLOW = FollowData(NICKNAME, NICKNAME_TO_FOLLOW)
        val FOLLOWING = listOf(NICKNAME_TO_FOLLOW)
        var TWEETS = listOf(TEXT)
    }
}
